# Pandora

Pandora: https://github.com/yandex/pandora

[Как писать сценарные пушки](https://youtu.be/lkusMkIniq0)

Типы пушек из коробки:

| gun | ammo            | protocol |
| --- |-----------------|----------|
| http| http/json, raw  | http1.1  |
| http2| http/json, raw  | http2  |
| connect|   | connect  |
| grpc|  grpc/json | GRPC  |

## Артефакты запуска

В папке `logs` лежат артефакты запуска:
 - `validated_conf.yaml` - Итоговый файл с конфигом, после всех преобразований. В yandex-tank есть механизм оверрайда конфигов. Можно построить свои конфиги на основе шаблонов, которые будут оверрайдится итоговым файлом. В нашем случае - мы оверрайдим дефолтную конфигурацию. Полная итоговая конфигурация - расположена в данном файле. 
 - `tank.log` - Логи самого yandex-tank - Бывает полезно сюда заглянуть. 
 - `pandora_****.log` - Логи Pandora генератора. Про конфиги пандоры - Яндекс-танк ничего не знает. Поэтому и проверить их не может. Если тест запускается и сразу стопается - смотреть надо именно сюда. Скорее всего - где-то ошибка в конфигах пандоры. 
 - `pandora_config_****.yaml` - Итоговый конфиш пандоры, который собрал Yandex-tank из итогового файла с конфигом.

# Дополнительные ссылки:

 - https://habr.com/ru/company/southbridge/blog/455290/
 - https://habr.com/ru/company/timeweb/blog/562378/
 - https://habr.com/ru/company/otus/blog/501978/

Про метрики go-grpc:
 - https://github.com/grpc-ecosystem/go-grpc-prometheus/blob/master/README.md
