package utils

import (
	"context"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// InitDialContext - инициализация grpc коннекта
func InitDialContext(ctx context.Context, target string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	log.Info().Msgf("Init GRPC connect to '%s'", target)

	grpcOpts := append(opts,
		grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultCallOptions(grpc.WaitForReady(true)),
		grpc.WithUserAgent("pandora load test"),
	)

	cc, err := grpc.DialContext(ctx, target, grpcOpts...)
	if err != nil {
		return nil, err

	}

	return cc, nil
}
