package demowallet

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/domain"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CreateAccount Создать счет
func (i *Implementation) CreateAccount(ctx context.Context, req *desc.CreateAccountRequest) (*desc.CreateAccountResponse, error) {
	acc := &domain.Account{
		AccountID:   uuid.NewString(),
		Amount:      req.GetAmount(),
		Description: req.GetDescription(),
	}
	err := i.store.AddAccount(ctx, acc)
	if err != nil {
		log.Error()
		return nil, status.Error(codes.Internal, fmt.Sprintf("Create account err: %v", err))
	}

	return &desc.CreateAccountResponse{
		AccountId:   acc.AccountID,
		Amount:      acc.Amount,
		Description: acc.Description,
	}, nil
}
