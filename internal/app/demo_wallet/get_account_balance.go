package demowallet

import (
	context "context"
	"fmt"

	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// GetAccountBalance получить инфу по балансу счета
func (i *Implementation) GetAccountBalance(ctx context.Context, req *desc.GetAccountBalanceRequest) (*desc.AccountBalanceResponse, error) {
	balance, err := i.store.GetAccount(ctx, req.GetAccountId())
	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Get balance err: %v", err))
	}

	return &desc.AccountBalanceResponse{
		AccountId: balance.AccountID,
		Amount:    balance.Amount,
	}, nil
}
