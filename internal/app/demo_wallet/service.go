package demowallet

import (
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/storage"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
)

// Implementation .
type Implementation struct {
	desc.UnimplementedDemoWalletServer
	store storage.Storage
}

// NewDemoWallet return new instance of Implementation.
func NewDemoWallet(store storage.Storage) *Implementation {
	return &Implementation{
		store: store,
	}
}
