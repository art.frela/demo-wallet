package demowallet

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
)

func TestCreateAccount(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		ctx := context.Background()

		impl := Implementation{store: store}

		req := &desc.CreateAccountRequest{
			Amount:      100,
			Description: "some desc",
		}

		resp, err := impl.CreateAccount(ctx, req)
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.NotEmpty(t, resp.GetAccountId())
		require.Equal(t, req.GetAmount(), resp.GetAmount())
		require.Equal(t, req.GetDescription(), resp.GetDescription())
	})
}
