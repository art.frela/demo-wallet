loadtest-images:
	docker pull grafana/grafana:latest
	docker pull victoriametrics/victoria-metrics:latest
	docker pull prom/node-exporter:latest
	docker pull yandex/yandex-tank:latest


loadtest: cfg=$cfg
loadtest:
	docker run \
		--rm -it \
		--network demo_wallet_ompnw \
		-v $(CURDIR)/loadtests/:/var/loadtest \
		yandex/yandex-tank:latest \
		-c "${cfg}"

loadtest-http:
	make loadtest cfg=http-pool/load.yaml

loadtest-grpc:
	make loadtest cfg=grpc-pool/load.yaml

loadtest-http-grpc:
	make loadtest cfg=http-grpc-pools/load.yaml

loadtest-custom-gun:
	make -f $(PWD)/loadtests/custom-gun/Makefile -C $(PWD)/loadtests/custom-gun/ generate-ammo
	make loadtest cfg=custom-gun/load.yaml
	#make loadtest cfg=custom-gun/load.autostop.yaml
